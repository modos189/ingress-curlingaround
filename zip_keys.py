import sys
from xml.dom.minidom import *

# генерация последней строки с количеством порталов у юзера
def gen_user_and_key(login, keys):
    return "<br/>@"+login+": "+keys+"</p>"

portals = {}
# перебор всех файлов котоые переданы аргументами
for file_name in sys.argv[1:]:
    login = file_name.split('_')[0]
    print('open '+file_name+" as login "+login)

    xml = parse(file_name)

    # список порталов
    document = xml.childNodes[0].childNodes[0].childNodes
    i = 0
    for node in document:

        # пропуск если это не инфа о портале
        if len(node.getElementsByTagName('Data')) < 3:
            continue

        # количество ключей от портала
        portal_keys = node.getElementsByTagName('Data')[0].firstChild.firstChild.nodeValue

        # получение GUID портала
        guid = node.getElementsByTagName('Data')[2].firstChild.firstChild.nodeValue

        # изменение описания портала
        # получение длинны последней строки чтоб удалить её и сгенерировать заново
        desc_endline_len = len(node.getElementsByTagName('description')[0].firstChild.nodeValue.split("<br/>")[-1:][0])+5
        desc_old = node.getElementsByTagName('description')[0].firstChild.nodeValue[:-desc_endline_len]
        desc_new = desc_old + gen_user_and_key(login, portal_keys)
        node.getElementsByTagName('description')[0].firstChild.nodeValue = desc_new

        # если этот портал уже содержится в хранилище
        if portals.get(guid, None) != None:

            cnt = int(portals[guid].getElementsByTagName('Data')[0].firstChild.firstChild.nodeValue) + int(portal_keys)
            portals[guid].getElementsByTagName('Data')[0].firstChild.firstChild.nodeValue = cnt

            another_desc_old = portals[guid].getElementsByTagName('description')[0].firstChild.nodeValue
            another_desc_old = another_desc_old[:-4]
            another_desc_new = another_desc_old + gen_user_and_key(login, portal_keys)

            portals[guid].getElementsByTagName('description')[0].firstChild.nodeValue = another_desc_new
            continue

        portals[guid] = node

print("Count portals: "+str(len(portals)))

impl = getDOMImplementation()

newdoc = impl.createDocument(None, "Document", None)
top_element = newdoc.documentElement

newname = impl.createDocument(None, "name", None)
name_element = newname.documentElement
text = newname.createTextNode('Portal keys')
name_element.appendChild(text)
top_element.appendChild(name_element)

for port in portals:
    top_element.appendChild(portals[port])

# сохранение в файл zip.kml
newdoc.writexml(open('zip.kml', 'w'), encoding='utf-8')
